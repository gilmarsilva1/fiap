variable "ecs-cluster-name" {
  default = "lab"
}

variable "memoria" {
  default = "256"
}

variable "cpu" {
  default = "512"
}

variable "region" {
  type = string
  default     = "us-east-1"
}

