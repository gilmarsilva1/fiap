provider "aws" {
  region  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile = "pessoal"
}

resource "aws_ecs_cluster" "lab-ecs" {
  name = var.ecs-cluster-name
}

resource "aws_ecs_task_definition" "task-app" {
  container_definitions = file("task-definitions/app.json")
  execution_role_arn = "arn:aws:iam::910018674439:role/ecsTaskExecutionRole"
  family = "service-app"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  cpu = "256"
  memory = "512"
}

resource "aws_ecs_task_definition" "task-db" {
  container_definitions = file("task-definitions/db.json")
  execution_role_arn = "arn:aws:iam::910018674439:role/ecsTaskExecutionRole"
  family = "service-db"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  cpu = "256"
  memory = "512"
}

resource "aws_ecs_service" "app" {
  name = "aplicacao"
  cluster = aws_ecs_cluster.lab-ecs.id
  desired_count = 1
  launch_type = "FARGATE"
  task_definition = aws_ecs_task_definition.task-app.id
  load_balancer {
    target_group_arn = "arn:aws:elasticloadbalancing:us-east-1:910018674439:targetgroup/target-lb-lab-fiap/f2032e60c1fb93b7"
    container_name = "app"
    container_port = 3000
  }
  network_configuration {
    subnets = ["subnet-01788856e344946bf", "subnet-0c912d5f9138164b2"]
    assign_public_ip = false
    security_groups = ["sg-0ea78882b0b2b051a", "sg-008136adf2f3e7bf9"]
  }
}

resource "aws_ecs_service" "db" {
  name = "bancodedados"
  cluster = aws_ecs_cluster.lab-ecs.id
  desired_count = 1
  launch_type = "FARGATE"
  task_definition = aws_ecs_task_definition.task-db.id
  load_balancer {
    target_group_arn = "arn:aws:elasticloadbalancing:us-east-1:910018674439:targetgroup/target-internal-db-2/97f80ec3aed9cc36"
    container_name = "db"
    container_port = 3306
  }
  network_configuration {
    subnets = ["subnet-04137bb5d1874524a", "subnet-0f7dc1194f2507513"]
    assign_public_ip = false
    security_groups = ["sg-008136adf2f3e7bf9","sg-0ea78882b0b2b051a"]
  }
}